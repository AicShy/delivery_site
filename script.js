let menu;
$('.when-loaded').hide();
function createApp(items, menu, admin) {
    return new Vue({
        delimiters: ['[[', ']]'],
        el: '#app',
        data: {
            items: items,
            currentPage: 0,
            itemsLimit: "10",
            filterType: "",
            filterAdmArea: "",
            filterDistrict: "",
            filterSocialPrivileges: "",
            filterIsNet: "",
            filterName: "",
            minSeats: "",
            maxSeats: "",
            dateStart: "",
            dateEnd:"",
            currentCafe: null,
            menu: menu,
            amount: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            fastDelivery: false,
            studentDiscount: false,
            hotOnly: false,
            admin: admin,
            operation: "new"
        },
        computed: {
            filteredItems: function () {
                let filtered = this.items;
                if (this.filterType)
                    filtered = filtered.filter(this.filterByType);
                if (this.filterAdmArea)
                    filtered = filtered.filter(this.filterByAdmArea);
                if (this.filterDistrict)
                    filtered = filtered.filter(this.filterByDistrict);
                if (this.filterSocialPrivileges)
                    filtered = filtered.filter(this.filterBySocialPrivileges);
                if (this.filterIsNet)
                    filtered = filtered.filter(this.filterByIsNet);
                if (this.filterName)
                    filtered = filtered.filter(this.filterByName);
                if (this.minSeats || this.maxSeats)
                    filtered = filtered.filter(this.filterBySeats);
                if (this.dateStart || this.dateEnd)
                    filtered = filtered.filter(this.filterByDate);
                return filtered;
            },
            currentItems: function () {
                let l = this.filteredItems.length;
                let maxPage = Math.ceil(l / this.itemsPerPage) - 1;
                if (maxPage < 0)
                    maxPage = 0;
                if (maxPage < this.currentPage)
                    this.currentPage = maxPage;
                let offset = this.itemsPerPage * this.currentPage;
                return this.filteredItems.slice(offset, offset + this.itemsPerPage);
            },
            itemsPerPage: function () {
                return parseInt(this.itemsLimit);
            },
            orderDisabled: function(){
                for (let i in this.amount){
                    if(this.amount[i] > 0){
                        return false;
                    }
                }
                return true;
            },
            maxRate: function() {
                // let res = 0;
                // for (i in this.items) {
                //     if (this.items[i].rate != null && this.items[i].rate > res)
                //         res = this.items[i].rate;
                // }
                // return res;
                return 100;
            },

            totalPrice:function(){
                if (!this.currentCafe)
                    return 0;
                let totalPrice = 0;
                for(i in this.amount){
                    totalPrice += this.currentCafe["set_" + (parseInt(i) + 1)] * this.amount[i];
                }
                if(this.fastDelivery)
                    totalPrice *= 1.2;
                if(this.studentDiscount)
                    totalPrice *= 0.9;
                return Math.floor(totalPrice);
            }
        },
        methods: {
            filterByType: function (item) {
                return item.typeObject == this.filterType;
            },
            filterByAdmArea: function (item) {
                return item.admArea == this.filterAdmArea;
            },
            filterByDistrict: function (item) {
                return item.district == this.filterDistrict;
            },
            filterBySocialPrivileges: function (item) {
                if(item.socialPrivileges == null)
                    return false;
                return item.socialPrivileges == this.filterSocialPrivileges;
            },
            filterByIsNet: function(item) {
                if(item.isNetObject == null)
                    return false;
                return item.isNetObject == this.filterIsNet;
            },
            filterByName: function(item) {
                return item.name.toLowerCase().indexOf(this.filterName.toLowerCase()) != -1;
            },
            filterBySeats: function(item) {
                if (item.seatsCount == null)
                    return false;
                let min = true;
                let max = true;
                if (this.minSeats) {
                    min = false;
                    if (parseInt(this.minSeats) <= item.seatsCount)
                        min = true;
                }   
                res = true;
                if (this.maxSeats) {
                    max = false;
                    if (parseInt(this.maxSeats) >= item.seatsCount)
                        max = true;
                }
                return min && max;
            },
            filterByDate: function(item) {
              if (!item.updated_at)
                return false;
              let after = true;
              let before = true;
              let dt = new Date(item.updated_at);
              if (this.dateStart) {
                let start = new Date(this.dateStart);
                if (dt < start)
                  after = false;
              }
              if (this.dateEnd) {
                let end = new Date(this.dateEnd);
                if (dt > end)
                  before = false;
              }
              return before && after;
            },
            newItemForm: function(){
                let newItem = {"id":null,"name":"","rate":0,"isNetObject":0,
                              "operatingCompany":"","typeObject":"","admArea":"Не выбрано",
                              "district":"Не выбрано","address":"","publicPhone":"",
                              "seatsCount":0,"socialPrivileges":0,"socialDiscount":0,
                              "created_at":null,"updated_at":null,
                              "set_1":1,"set_2":2,"set_3":3,"set_4":4,"set_5":5,
                              "set_6":6,"set_7":7,"set_8":8,"set_9":9,"set_10":10};
                this.operation = 'new';
                this.currentCafe = newItem;
            },
            removeItemById: function(id) {
              let newItems = [];
              let currentItems = this.items;
              for (let i in currentItems) {
                //console.log(i);
                //console.log(currentItems[i]);
                if (currentItems[i] !== undefined && currentItems[i].id !== undefined && currentItems[i].id != id)
                  newItems.push(currentItems[i]);
              }
              this.items = newItems;
            },
            removeCurrentItem: function() {
              //console.log('>>');
              //console.log(this.currentCafe);
              this.removeItemById(this.currentCafe.id);
            }
        }
    });
}



Vue.component('item-cell', {
    delimiters: ['[[', ']]'],
    props: ['item'],
    template: `<tr>
    <td>[[ item.name ]]</td>
    <td>[[ item.typeObject ]]</td>
    <td>[[ item.address ]]</td>

    <td v-if="isAdmin">
        <div class="d-flex flex-row flex-nowrap align-items-center justify-content-between">
            <img src="s1.svg" alt="" data-toggle="modal" data-target="#viewModal" @click="setCurrent('none')">
            <img src="s2.svg" alt="" data-toggle="modal" data-target="#newModal" @click="setCurrent('edit')">
            <img src="s3.svg" alt=""data-toggle="modal" data-target="#deleteModal" @click="setCurrent">
        </div>
    </td>

    <td v-else><button type="button" class="btn btn-success" :id="'search-' + this.item.id" @click="setCurrent">Выбрать</button></td></tr>`,
    computed:{
        isAdmin: function(){
            return this.$root.admin;
        }
    },
    methods: {
        setCurrent: function (op) {
            if(this.$root.currentCafe != this.item){
                this.$root.currentCafe = this.item;
                let newAmounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                this.$root.amount = newAmounts;
                this.$root.operation = op;
            }
        }
    }
});

Vue.component('item-table-cell', {
    delimiters: ['[[', ']]'],
    template: `<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Название</th>
        <th scope="col">Тип</th>
        <th scope="col">Адрес</th>
        <th scope="col">Выбрать</th>
      </tr>
    </thead>
    <tbody id="table">
      <template v-for="item in currentItems">
        <item-cell :item="item"></item-cell>
      </template>
    </tbody>
    </table>`,
    computed: {
        currentItems: function () {
            return this.$root.currentItems;
        }
    }
});

Vue.component("pages-cell", {
    delimiters: ['[[', ']]'],
    template: `<nav aria-label="...">
     <ul class="pagination">
       <li class="page-item" v-if="currentPage > 0">
         <a class="page-link" tabindex="-1" @click="return toPage(0)">First</a>
       </li>
       <li class="page-item" v-if="currentPage > 0"><a @click="return toPrev()" class="page-link">[[ displayPages.prev ]]</a></li>
       <li class="page-item active" aria-current="page">
         <a class="page-link"><span>[[ displayPages.curr ]]</span></a>
       </li>
       <li class="page-item" v-if="currentPage < maxPages - 1"><a @click="return toNext()" class="page-link">[[ displayPages.next ]]</a></li>
       <li class="page-item" v-if="currentPage < maxPages - 1">
         <a class="page-link" @click="return toPage( maxPages - 1)">Last</a>
       </li>
     </ul>
   </nav>`,
    computed: {
        currentPage: function () {
            return this.$root.currentPage;
        },
        maxPages: function () {
            let l = this.$root.filteredItems.length;
            return Math.ceil(l / this.$root.itemsPerPage);
        },
        displayPages: function () {
            return { 'prev': this.currentPage, 'curr': this.currentPage + 1, 'next': this.currentPage + 2 };
        }
    },
    methods: {
        toPrev: function () {
            this.$root.currentPage--;
            return false;
        },
        toNext: function () {
            this.$root.currentPage++;
            return false;
        },
        toPage: function (num) {
            this.$root.currentPage = num;
            return false;
        }

    }
});

Vue.component('type-select-cell', {
    delimiters: ['[[', ']]'],
    template: `<div class="col-sm-6 col-lg-4 mb-3">
    <h5>Тип</h5>
    <select class="form-control" id="type" :model="filterType" @input="updateFilter($event.target.value)">
      <option value="">Не выбрано</option>
      <option v-for="tp in typeList" :value="tp">[[ tp ]]</option>
    </select>
  </div>`,
    computed: {
        typeList: function () {
            let res = [];
            let uniq = {};
            let i;
            for (i in this.$root.items) {
                if (this.$root.items[i].typeObject)
                    uniq[this.$root.items[i].typeObject] = 1;
            }
            for (i in uniq) {
                res.push(i);
            }
            res.sort();
            return res;
        },
        filterType: function () {
            return this.$root.filterType;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.filterType = val;
        }
    }
});


Vue.component('admarea-select-cell', {
    delimiters: ['[[', ']]'],
    template: `<div class="col-sm-6 col-lg-6 mb-3">
    <h5>Административный округ</h5>
    <select class="form-control" id="type" :model="filterAdmArea" @input="updateFilter($event.target.value)">
      <option value="">Не выбрано</option>
      <option v-for="tp in typeList" :value="tp">[[ tp ]]</option>
    </select>
  </div>`,
    computed: {
        typeList: function () {
            let res = [];
            let uniq = {};
            let i;
            for (i in this.$root.items) {
                if (this.$root.items[i].admArea)
                    uniq[this.$root.items[i].admArea] = 1;
            }
            for (i in uniq) {
                res.push(i);
            }
            res.sort();
            return res;
        },
        filterAdmArea: function () {
            return this.$root.filterAdmArea;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.filterAdmArea = val;
        }
    }
});

Vue.component('district-select-cell', {
    delimiters: ['[[', ']]'],
    template: `
    <div class="col-sm-6 col-lg-6 mb-3">
    <h5>Район</h5>
    <select class="form-control" id="type" :model="filterDistrict" @input="updateFilter($event.target.value)">
      <option value="">Не выбрано</option>
      <option v-for="tp in typeList" :value="tp">[[ tp ]]</option>
    </select>
  </div>`,
    computed: {
        typeList: function () {
            let res = [];
            let uniq = {};
            let i;
            for (i in this.$root.items) {
                if (this.$root.items[i].district)
                    uniq[this.$root.items[i].district] = 1;
            }
            for (i in uniq) {
                res.push(i);
            }
            res.sort();
            return res;
        },
        filterDistrict: function () {
            return this.$root.filterDistrict;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.filterDistrict = val;
        }
    }
});

Vue.component('socialprivileges-select-cell', {
    delimiters: ['[[', ']]'],
    template: `
    <div class="col-sm-6 col-lg-4 mb-3">
    <h5>Соц.скидки</h5>
    <select class="form-control" id="type" :model="filterSocialPrivileges" @input="updateFilter($event.target.value)">
      <option value="">Не выбрано</option>
      <option v-for="tp in typeList" :value="tp">[[tp|boolToStr]]</option>
    </select>
  </div>`,
  filters: {
    boolToStr: function(value){
        return ["нет","да"][parseInt(value)];
    }
    },
    computed: {
        typeList: function () {
            return [0, 1]
        },
        filterSocialPrivileges: function () {
            return this.$root.filterSocialPrivileges;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.filterSocialPrivileges = val;
        }
    }
})

Vue.component('isnet-select-cell', {
    delimiters: ['[[', ']]'],
    template: `
    <div class="col-sm-12 col-lg-6 mb-3">
    <h5>Является сетевым</h5>
    <select class="form-control" id="type" :model="filterIsNet" @input="updateFilter($event.target.value)">
      <option value="">Не выбрано</option>
      <option v-for="tp in typeList" :value="tp">[[ labels[tp] ]]</option>
    </select>
  </div>`,
    computed: {
        typeList: function () {
            return [0, 1]
        },
        labels: function () {
            return ["нет", "да"]
        },
        filterIsNet: function () {
            return this.$root.filterIsNet;
        }
    },
    methods: {
        updateFilter: function (val) {
            this.$root.filterSocialPrivileges = val;
        }
    }
})

Vue.component('menu-item-cell', {
    delimiters: ['[[', ']]'],
    props: ["menuItem", "index"],
    template: `
    <div class="col-sm-6 col-lg-4">
      <div class="card mb-3">
        <img :src="menuItem.img" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">[[menuItem.name]]</h5>
          <p class="card-text">[[menuItem.description]]</p>
          <h5 class="text-center">[[currentPrice]]&nbsp;₽</h5>
          <div class="d-flex justify-content-around align-items-center addning">
            <button type="button" :disabled="currentAmount == 0" class="btn btn-success change" @click = "decAmount()">-</button>
            <input class="form-control amount" :value="currentAmount" type="number" id="value" placeholder="0" :model="currentAmount" @input="updateAmount($event.target.value)">
            <button type="button" class="btn btn-success change" @click="incAmount()">+</button>
          </div>
        </div>
      </div>
    </div>
    `,
    computed: {
        pos: function () {
            return parseInt(this.index) - 1;
        },
        currentPrice: function () {
            return this.$root.currentCafe["set_" + this.index]
        },
        currentAmount: function () {
            return this.$root.amount[this.pos];
        }
    },
    methods: {
        updateAmount: function (amount) {
            let newAmount = [...this.$root.amount];
            newAmount[this.pos] = amount;
            this.$root.amount = newAmount;
        },
        incAmount: function () {
            let newAmount = [...this.$root.amount];
            newAmount[this.pos]++;
            this.$root.amount = newAmount;
        },
        decAmount: function () {
            let newAmount = [...this.$root.amount];
            newAmount[this.pos]--;
            this.$root.amount = newAmount;
        }
    }
});



Vue.component('menu-cell', {
    delimiters: ['[[', ']]'],
    template: `
    <div class="mb-5 p-3" style="background-color: #ffffff; border: 1px solid rgba(0,0,0,.125); border-radius: 5px;">
    <h2 class="mb-3 text-center">Меню</h2>
    <div class="row">
        <menu-item-cell v-for = "(menuItem, id) in menu" :menuItem = "menuItem" :index = "id + 1" ></menu-item-cell>
    </div>
    </div>
    `,
    computed: {
        menu: function () {
            return this.$root.menu;
        }
    }
})

Vue.component('menu-choice-cell',{
    delimiters: ['[[', ']]'],
    props: ["item_id"],
    template:`
    <div class="card mb-3" style="max-width: 540px;">
    <div class="row no-gutters d-flex align-items-center">
      <div class="col-md-4 d-none d-md-block">
        <img :src="pos.img" class="card-img" alt="...">
      </div>
      <div class="col-md-8">
        <div class="card-body pb-0">
          <div class="d-flex justify-content-between align-items-center">
            <p class="card-text col-6">[[pos.name]]</p>
            <p class="card-text">[[count]]x[[price]]</p>
            <p class="card-text">[[fullPrice]]&nbsp;₽</p>
          </div>
        </div>
      </div>
    </div>
  </div>
    `,
    computed:{
        pos: function(){
            return this.$root.menu[parseInt(this.item_id)];
        },
        price: function(){
            return this.$root.currentCafe["set_" + (parseInt(this.item_id) + 1)];
        },
        count: function(){
            return this.$root.amount[parseInt(this.item_id)];
        },
        fullPrice: function(){
            return this.price * parseInt(this.count); 
        }
    }
})

Vue.component('menu-choice',{
    delimiters: ['[[', ']]'],
    template:`
    <div class="mb-3">
    <h4 class=" border-bottom mb-2">Позиции заказа</h4>
    <div>
        <menu-choice-cell v-for = "item in orders" :item_id = "item"></menu-choice-cell>
    </div>
    </div>
    `,
    computed:{
        orders: function(){
            let res = [];
            for (let i in this.$root.amount){
                if (this.$root.amount[i] > 0)
                    res.push(i);
            }
            return res;
        }
    }

})

Vue.component('cafe-description', {
    delimiters: ['[[', ']]'],
    template: `<div class="mb-3">
    <h5 class="border-bottom mb-2">Информация о предприятии</h5>
    <div class="d-flex flex-column">
      <div class=" row d-flex flex-row flex-wrap  justify-content-between mb-1" v-if="item.name != null">
        <div class="col-md-6 col-sm-12">
          <h6 class="mt-1">Название:</h6>
        </div>
        <div class="col-md-6 col-sm-12">
          [[item.name]]
        </div>
      </div>

      <div class="row d-flex flex-row flex-wrap justify-content-between mb-1"  v-if="item.admArea != null">
        <div class="col-md-6 col-sm-12">
          <h6 class="mt-1 text-break">Административный округ:</h6>
        </div>
        <div class="col-md-6 col-sm-12">
        [[item.admArea]]
        </div>
      </div>

      <div class=" row d-flex flex-row flex-wrap  justify-content-between mb-1"  v-if="item.district != null">
        <div class="col-md-6 col-sm-12">
          <h6 class="mt-1">Район:</h6>
        </div>
        <div class="col-md-6 col-sm-12">
        [[item.district]]
        </div>
      </div>

      <div class=" row d-flex flex-row flex-wrap  justify-content-between mb-1" v-if="item.address != null">
        <div class="col-md-6 col-sm-12">
          <h6 class="mt-1">Адрес:</h6>
        </div>
        <div class="col-md-6 col-sm-12 text-black-50">
        [[item.address]]
        </div>
      </div>

      <div class="row d-flex flex-row flex-wrap  justify-content-between " v-if="item.rate != null">
        <div class="col-md-6 col-sm-12 align-items-end ">
          <h6 class="mt-1">Рейтинг:</h6>
        </div>
        <div class="col-md-6 col-sm-12 d-flex flex-row align-items-end">
        [[item.rate]]
        <template v-for="star in parseInt(stars)">
          <svg class="bi bi-star mb-1 ml-2" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
            xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd"
              d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.523-3.356c.329-.314.158-.888-.283-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767l-3.686 1.894.694-3.957a.565.565 0 0 0-.163-.505L1.71 6.745l4.052-.576a.525.525 0 0 0 .393-.288l1.847-3.658 1.846 3.658a.525.525 0 0 0 .393.288l4.052.575-2.906 2.77a.564.564 0 0 0-.163.506l.694 3.957-3.686-1.894a.503.503 0 0 0-.461 0z" />
          </svg>
        </template>
        </div>
      </div>
    </div>
  </div>`,
    computed:{
        item: function(){
            return this.$root.currentCafe;
        },
        stars: function() {
            //console.log(this.$root.maxRate);
            //console.log(this.$root.currentCafe.rate);
            if (this.$root.currentCafe.rate == null)
                return 0;
           // console.log(Math.ceil((this.$root.currentCafe.rate / this.$root.maxRate) * 5));
            return  Math.ceil((this.$root.currentCafe.rate / this.$root.maxRate) * 5);
        }
    },
    methods: {
        setCurrent: function () {
            if(this.$root.currentCafe != this.item){
                this.$root.currentCafe = this.item;
                let newAmounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                this.$root.amount = newAmounts;
            }
        }
    }
})


Vue.component('total-sum',{
    delimiters: ['[[', ']]'],
    template:`
    <div class="row d-flex flex-row flex-wrap  justify-content-between align-items-center">
    <div class="col-md-6 col-sm-12">
      <h5 class="mt-1">Итого:</h5>
    </div>
    <div class="col-md-6 col-sm-12">
      [[totalPrice]]&nbsp;₽
    </div>  
  </div>
    `,
    computed:{
        totalPrice: function() {
            return this.$root.totalPrice + 250;
        }
    }
});

Vue.component('modal-view',{
    delimiters: ['[[', ']]'],
    template:`
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">[[item.name]]</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-4 d-flex flex-column flex-nowrap">
        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Название:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.name]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Является сетевым:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.isNetObject|boolToStr]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Название управляющей компании:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[nameOfCompany]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Вид объекта:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.typeObject]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Административный округ:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.admArea]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Район:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.district]]</h6>
          </div>
        </div>


        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Адрес:</h5>
          <div class="col-md-6 col-sm-12">
            <h6 class="row col-12 text-black-50">[[item.address]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Число посадочных мест:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.seatsCount]]</h6>
          </div>
        </div>

        <div class="mb-3 d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Наличие социальных льгот:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-2">[[item.socialPrivileges|boolToStr]]</h6>
          </div>
        </div>

        <div class=" d-flex flex-row flex-wrap">
          <h5 class="col-md-6 col-sm-12">Контактный телефон:</h5>
          <div class="row col-md-6 col-sm-12">
            <h6 class="col-12">[[item.publicPhone]]</h6>
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
      </div>
    </div>`,
filters: {
    boolToStr: function(value){
        return ["нет","да"][parseInt(value)];
    }
},
computed:{
    item: function(){
        return this.$root.currentCafe;
    },
    nameOfCompany: function(){
        if(this.item.operatingCompany)
            return this.item.operatingCompany
        return "Неизвестно"
    }
}
})

Vue.component('modal-edit',{
delimiters: ['[[', ']]'],
template:`
<div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Создание/редактирование записи</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body px-4 d-flex flex-column flex-nowrap">
          <div class="mb-3">
            <h5 class="mb-2">Название</h5>
            <div>
              <input class="form-control" type="text" :value="item.name" @blur="updateCurrent($event.target.value, 'name')">
            </div>
          </div>

          <div class="mb-3">
            <h5 class="mb-2">Является сетевым</h5>
            <select class="form-control" @input="updateCurrent($event.target.value, 'isNetObject')">
            <option v-for="tp in typeList('isNetObject')" :value="tp" :selected="tp == item.isNetObject">[[tp|boolToStr]]</option>
            </select>
          </div>

          <div class="mb-3">
            <h5 class="mb-2">Название управляющей компании</h5>
            <div>
              <input class="form-control" type="text" :value="item.operatingCompany" @blur="updateCurrent($event.target.value, 'operatingCompany')">
            </div>
          </div>

          <div class="mb-3">
            <h5 class="mr-3">Вид объекта</h5>
            <select class="form-control" @input="updateCurrent($event.target.value, 'typeObject')">
            <option v-for="tp in typeList('typeObject')" :value="tp" :selected="tp == item.typeObject">[[tp]]</option>
            </select>
          </div>

          <div class="mb-3">
            <h5 class="mr-3">Административный округ</h5>
            <select class="form-control" @input="updateCurrent($event.target.value, 'admArea')">
            <option v-for="tp in typeList('admArea')" :value="tp" :selected="tp == item.admArea" >[[tp]]</option>
            </select>
          </div>

          <div class="mb-3">
            <h5 class="mr-3">Район</h5>
            <select class="form-control" @input="updateCurrent($event.target.value, 'district')">
            <option v-for="tp in typeList('district')" :value="tp" :selected="tp == item.district" >[[tp]]</option>
            </select>
          </div>

          <div class="mb-3">
            <h5 class="mb-2">Адрес</h5>
            <div>
              <input class="form-control" type="text" :value="item.address" @blur="updateCurrent($event.target.value, 'address')">
            </div>
          </div>

          <div class="mb-3">
            <h5 class="mb-2">Число посадочных мест</h5>
            <div>
              <input class="form-control no" type="numder" :value="item.seatsCount" @blur="updateCurrent($event.target.value, 'seatsCount')">
            </div>
          </div>

          <div class="mb-3">
            <h5 class="mb-2">Показатель социальных льгот</h5>
            <select class="form-control" @input="updateCurrent($event.target.value, 'socialPrivileges')">
            <option v-for="tp in typeList('socialPrivileges')" :value="tp" :selected="tp == item.socialPrivileges" >[[tp|boolToStr]]</option>
          </select>
        </div>

          <div class="mb-3">
            <h5 class="mb-2">Телефон</h5>
            <div>
            <input class="form-control no" type="text" :value="item.publicPhone" @blur="updateCurrent($event.target.value, 'publicPhone')">
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" @click="processEditForm">Сохранить</button>
        </div>
      </div>
`,
filters: {
    boolToStr: function(value){
        return ["нет","да"][parseInt(value)];
    }
},
computed:{
    item: function(){
        //console.log(this.$root.currentCafe);
        return this.$root.currentCafe;
    },
    nameOfCompany: function(){
        if(this.item.operatingCompany)
            return this.item.operatingCompany
        return ""
    }
},
methods:{
    typeList: function (find) {
        let res = [];
        let uniq = {};
        let i;
        for (i in this.$root.items) {
            if (this.$root.items[i][find] != null)
                uniq[this.$root.items[i][find]] = 1;
        }
        for (i in uniq) {
            res.push(i);
        }
        res.sort();
        return res;
    },
    updateCurrent: function(value, field) {
      this.$root.currentCafe[field] = value;
    },
    processEditForm: function() {
      if(this.$root.operation == 'new') {
        this.$root.items.push(this.$root.currentCafe);
        addItem(this.$root.currentCafe);
      } else {
        updateItem(this.$root.currentCafe);
      }
    }
}
})
Vue.component('delete-model',{
    delimiters: ['[[', ']]'],
    template:`
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Удаление записи</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body px-4 d-flex flex-column flex-nowrap">

            <div class="col-12 d-flex flex-row flex-wrap">
              <h6>Вы уверены, что хотите удалить данные предприятия [[item.name]] ?</h6>
            </div>

          </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="removeCurrentItem">Удалить</button>
      </div>
    </div>
    `,
    computed:{
        item: function(){
            return this.$root.currentCafe;
        }
    },
    methods: {
      removeCurrentItem: function() {
        //console.log(this.$root.currentCafe);
        deleteItem(this.$root.currentCafe.id);
        this.$root.removeCurrentItem();
      }
    }
})

const app = function (items, menu) {
    return createApp(items, menu, isAdmin);
}

$(document).ready(function () {

    jQuery.get("/food.json", {}, function (data) {
        menu = data;
        
        jQuery.ajax({
            url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1',
            type: 'GET',
            crossDomain: true,
            success: function (data) {
                $('.loading-spinner').hide();
                $(`#records`).show();
                $('.when-loaded').show();
                app(data, menu);
            }

        });
    })
});

function addItem(item) {
  $.ajax({
    url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1',
    type: 'POST',
    crossDomain: true,
    data: item,
    success: function(data) {
      item.id = data.id;
      item.created_at = data.created_at;
      item.updated_at = data.updated_at;
    },
    error: function(x, textstatus, error) {
      alert("Ошибка во время добавления записи! Ответ сервера: " + textstatus + ". Ошибка: "+error);
    }
   });
}

function updateItem(item) {
  $.ajax({
    url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1/' + item.id,
    type: 'PUT',
    crossDomain: true,
    data: item,
    success: function(data) {
      item.updated_at = data.updated_at;
    },
    error: function(x, textstatus, error) {
      alert("Ошибка во время редактирования записи! Ответ сервера: " + textstatus + ". Ошибка: "+error);
    }
   });
}

function deleteItem(id) {
  $.ajax({
    url: 'http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1/' + id,
    type: 'DELETE',
    crossDomain: true,
    error: function(x, textstatus, error) {
      alert("Ошибка во время удаления записи! Ответ сервера: " + textstatus + ". Ошибка: "+error);
    }
   });
}